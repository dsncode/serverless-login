module gitlab.com/dsncode/serverless-login

go 1.12

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.1
)
