package service

import (
	"testing"

	"github.com/google/uuid"
	"gitlab.com/dsncode/serverless-login/model"
)

var (
	service AuthenticationService
)

func init() {
	users := make(map[string]string)
	users["daniel"] = "123123"
	service = NewInMemoryAutenticatorService(users)
}
func TestAuthentication(t *testing.T) {

	loginRequest := model.LoginRequest{
		Username: "daniel",
		Password: "123123",
	}

	response, _ := service.Authenticate(loginRequest)
	if response.Code != 200 {
		t.Fatal("authentication should not fail for these values")
	}

	_, err := uuid.Parse(response.Token)
	if err != nil {
		t.Fatal("wrong token format")
	}

	loginRequest = model.LoginRequest{
		Username: "someone",
		Password: "123123",
	}

	response, _ = service.Authenticate(loginRequest)
	if response.Code != 403 {
		t.Fatalf("authentication must fail for these values")
	}

}
