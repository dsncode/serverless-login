package service

import (
	"github.com/google/uuid"
	"gitlab.com/dsncode/serverless-login/model"
)

type AuthenticationService interface {
	Authenticate(request model.LoginRequest) (model.LoginResponse, error)
}

// InMemoryAuthenticationService creates a in memory service to authenticate users
type InMemoryAuthenticationService struct {
	users map[string]string
}

func NewInMemoryAutenticatorService(referenceUsers map[string]string) (service AuthenticationService) {

	service = &InMemoryAuthenticationService{
		users: referenceUsers,
	}
	return
}

// Authenticate users on memory
func (service *InMemoryAuthenticationService) Authenticate(request model.LoginRequest) (response model.LoginResponse, err error) {

	if val, ok := service.users[request.Username]; ok && val == request.Password {
		uuid := uuid.New()
		response.Token = uuid.String()
		response.Code = 200
		return
	}

	// this must be server error
	if err != nil {
		response.Message = err.Error()
		response.Code = 500
	} else {
		//user is invalid
		response.Code = 403
		response.Message = "Wrong credentials"
	}

	return
}
