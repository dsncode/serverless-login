package model

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Code    int    `json:"code"`
	Token   string `json:"token,omitempty"`
	Message string `json:"message,omitempty"`
}

type HealtCheckResponse struct {
	Message string `json:"messsage"`
}
