package autentication

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/dsncode/serverless-login/model"
	"gitlab.com/dsncode/serverless-login/service"
)

var (
	g                     *gin.Engine
	authenticationService service.AuthenticationService
)

func init() {
	log.Println("Initializing authentication controller")

	//TODO: replace for firebase implementation
	users := make(map[string]string)
	users["daniel"] = "222"
	authenticationService = service.NewInMemoryAutenticatorService(users)

	g = gin.New()
	g.POST("/login", login)
	g.GET("/", healthcheck)
}

func healthcheck(ctx *gin.Context) {

	var response model.HealtCheckResponse

	response.Message = "online"
	ctx.JSON(200, response)

}

func login(ctx *gin.Context) {
	var loginRequest model.LoginRequest
	ctx.BindJSON(&loginRequest)

	response, err := authenticationService.Authenticate(loginRequest)
	if err != nil {
		ctx.JSON(500, gin.H{
			"message": "server error",
		})
		return
	}

	ctx.JSON(response.Code, response)

}

// Handler is our cloud function entry point
func Handler(w http.ResponseWriter, r *http.Request) {
	g.ServeHTTP(w, r)
}
