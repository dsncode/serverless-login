# Authentication Serverless - Google Cloud Platform v 0.1.0

Written by daniel Silva daniel@dsncode.com

## How to deploy

### Via Google Cloud UI Console

1. Please zip all the relevant files 
```bash
zip -r autenticator.zip . -x "./info/*" "./.git/*" "README.md" ".gitignore" 
```

### 2. Registration

2.1. Register this information as shown bellow. you can choose any region as you need

![login](info/step-1.jpg)


2.2 Upload your file

![login](info/step-2.jpg)

### 3. Check if function is online

check trigger entry point. you should see the content for this function healthcheck.

